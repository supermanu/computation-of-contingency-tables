# How faster can contingency tables be computer for GWAS?

This repositary gives access to codes in R and C to compute contingency tables for genome wide association study. Naive and optimized approaches are utilized to compute the contingency tables.  
Mode details are available at https://emmanuelssite.000webhostapp.com/Contengency.html

## Historic

This personal project was initialized between 2012 and 2014. I recently create this repository to make my codes accesible online. I would be happy to discussed about it.   

## Compilation

Codes in C were compiled with x86_64-w64-mingw32-gcc.exe on Windows 10 pro. 
The loops require a lot of room in the stack so the linker option '-Wl,--stack,2500000000' or more may be required depending on the machine and the parameters. 
The code is portable and be be compile on any platform.
Codes in R are pretty straitghforward to be used. 

### Prerequisites

A C compiler needs to be installed on your machine. 
Also I have to say that expert knowledge may be required ... Sorry !

## Authors

* **Emmanuel Sapin** - *Personal project*