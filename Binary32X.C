#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <limits.h>
#include <math.h>
#include <inttypes.h>

#define MAXINDIV 1201

#define MAXGENE MAXINDIV*100

#define SIZETYPE 32

#if SIZETYPE==64
#define UNCORING uint64_t
#define COUNTNBBITS popcount64a

#endif

#if SIZETYPE==32
#define UNCORING uint32_t
#define COUNTNBBITS popcount32a
#endif


const uint64_t m1  = 0x5555555555555555; //binary: 0101....
const uint64_t m2  = 0x3333333333333333; //binary: 00110011..
const uint64_t m4  = 0x0f0f0f0f0f0f0f0f; //binary:  4 zeros,  4 ones ...
const uint64_t m8  = 0x00ff00ff00ff00ff; //binary:  8 zeros,  8 ones ...
const uint64_t m16 = 0x0000ffff0000ffff; //binary: 16 zeros, 16 ones ...
const uint64_t m32 = 0x00000000ffffffff; //binary: 32 zeros, 32 ones
const uint64_t hff = 0xffffffffffffffff; //binary: all ones
const uint64_t h01 = 0x0101010101010101; //the sum of 256 to the power of 0,1,2,3...


const uint32_t m1b32  = 0x55555555; //binary: 0101...
const uint32_t m2b32  = 0x33333333; //binary: 00110011..
const uint32_t m4b32  = 0x0f0f0f0f; //binary:  4 zeros,  4 ones ...
const uint32_t m8b32  = 0x00ff00ff; //binary:  8 zeros,  8 ones ...
const uint32_t m16b32 = 0x0000ffff; //binary: 16 zeros, 16 ones ...
const uint32_t hffb32 = 0xffffffff; //binary: all ones
const uint32_t h01b32 = 0x01010101; //the sum of 256 to the power of 0,1,2,3...

//unsigned char tab[MAXGENE][(MAXINDIV-1)/8+1];
    //could be  improved

int savefile(int indiv,char casesfile[],UNCORING** tabtosave)
{   int nbgene=100*indiv;
   /* for(int cgene=0;cgene<nbgene;cgene++)
    {   for(int cindiv=0;cindiv<((indiv-1)/SIZETYPE+1);cindiv++)
        {   printf("%llu ",tabtosave[cgene][cindiv]/256/256/256);
            printf("%llu ",(tabtosave[cgene][cindiv]/256/256)%256);
            printf("%llu ",((tabtosave[cgene][cindiv]/256)%256)%256);
            printf("%llu ",tabtosave[cgene][cindiv]%256);
        };
        printf("\n");
    };*/
   /* for(int cgene=0;cgene<nbgene;cgene++)
    {   for(int cindiv=0;cindiv<((indiv-1)/8+1);cindiv++)
        {   int place = cindiv*8/SIZETYPE;
            int octet   = cindiv % (SIZETYPE/8);
            tab[cgene][cindiv] = (tabtosave[cgene][place] & (255<<(octet*8)))>>(octet*8);
        };
    };*/
    FILE * file;
    if ((file = fopen(casesfile, "w+")) == NULL) return 1;
    char gstring[2];
    for(int cgene=0;cgene<nbgene;cgene++)
    {   //printf ("%d",cgene);
        for(int cindiv = 0;cindiv<4*((indiv-1)/8+1);cindiv++)
        {   int place = cindiv*8/SIZETYPE;
            int octet   = cindiv % (SIZETYPE/8);
          //  printf("%d,%d ",place,octet);
            gstring[0] = (tabtosave[cgene][place] & (255<<(octet*8)))>>(octet*8);
            fwrite (gstring,1,1,file);
        };
    };
    fclose (file);
}

int generatedatabinary(int indiv,UNCORING** cases
                                ,UNCORING** controls) // int *cases,int *controls)
{    printf("strat initialisation");
	int nbgene=100*indiv;
    for(int cgene=0;cgene<nbgene;cgene++)
    { //  printf("\n %d,%d,%llu,%llu,%llu",cgene,((indiv-1)/SIZETYPE+1),caseCH[cgene][((indiv-1)/SIZETYPE+1)-1],caseHH[cgene][((indiv-1)/SIZETYPE+1)-1],caseRH[cgene][((indiv-1)/SIZETYPE+1)-1]);
        // printf("\n ,%llu,%llu,%llu,%llu,%llu ", caseUN[cgene][((indiv-1)/SIZETYPE+1)-1],controlCH[cgene][((indiv-1)/SIZETYPE+1)-1],controlHH[cgene][((indiv-1)/SIZETYPE+1)-1],controlRH[cgene][((indiv-1)/SIZETYPE+1)-1],
          //      controlUN[cgene][((indiv-1)/SIZETYPE+1)-1] );
        for(int cindiv=0;cindiv<4*((indiv-1)/SIZETYPE+1);cindiv++)
        {   cases[cgene][cindiv]    =0;
            controls[cgene][cindiv] =0;
        };
    };
    printf("initialisation okay");
    for(int cgene=0;cgene<nbgene;cgene++)
    {   for(int cindiv=0;cindiv<indiv;cindiv++)
        {   int place = (cindiv)/SIZETYPE;
            int bit   = cindiv % SIZETYPE;
            int SNP=rand() % 4;
            cases[cgene][place+SNP*((indiv-1)/SIZETYPE+1)]   = cases[cgene][place+SNP*((indiv-1)/SIZETYPE+1)] | (1<<bit);
            SNP=rand() % 4;
            controls[cgene][place+SNP*((indiv-1)/SIZETYPE+1)]= controls[cgene][place+SNP*((indiv-1)/SIZETYPE+1)] | (1<<bit);
        };
    };
    printf("affectation okay");
    char filename[100];
    char number[100];
    sprintf(number, "%d", indiv);
    strcpy(filename,"cases");strcat(filename,number);strcat(filename,".data");savefile(indiv,filename,cases);
    printf("frist save okay");
    strcpy(filename,"controls");strcat(filename,number);strcat(filename,".data");savefile(indiv,filename,controls);
    return (0);
}

int loadfile(int indiv,char casesfile[],UNCORING** tabtoload)
{   int nbgene=100*indiv;
    FILE * file;
    if ((file = fopen(casesfile, "r")) == NULL) return 1;
    char gstring[2];
    for(int cgene=0;cgene<nbgene;cgene++)
    {   for(int cindiv = 0;cindiv<4*((indiv-1)/8+1);cindiv++)
        {   fread( gstring, 1,1,file);
            int octet=cindiv%(SIZETYPE/8);
            int place=cindiv*8/SIZETYPE;
            //printf("octet%d,place%d",octet,place);
            tabtoload[cgene][place]    =  tabtoload[cgene][place] | (gstring[0] << (octet*8));
        }
    }
    fclose (file);
}

int loadcasecontrolbinary(int indiv,UNCORING** cases
                                ,UNCORING** controls)
{   int nbgene=100*indiv;
    char filename[100];
    char number[100];
    sprintf(number, "%d", indiv);
    strcpy(filename,"cases");strcat(filename,number);strcat(filename,".data");loadfile(indiv,filename,cases);
    strcpy(filename,"controls");strcat(filename,number);strcat(filename,".data");loadfile(indiv,filename,controls);
    return 0;
}

int popcount64a(uint64_t x)
{   x = (x & m1) + ((x >>  1) & m1 ); //put count of each  2 bits into those  2 bits
    x = (x & m2 ) + ((x >>  2) & m2 ); //put count of each  4 bits into those  4 bits
    x = (x & m4 ) + ((x >>  4) & m4 ); //put count of each  8 bits into those  8 bits
    x = (x & m8 ) + ((x >>  8) & m8 ); //put count of each 16 bits into those 16 bits
    x = (x & m16) + ((x >> 16) & m16); //put count of each 32 bits into those 32 bits
    x = (x & m32) + ((x >> 32) & m32); //put count of each 64 bits into those 64 bits
    return x;
}

int popcount32a(uint32_t x)
{   x = (x & m1b32 ) + ((x >>  1) & m1b32 ); //put count of each  2 bits into those  2 bits
    x = (x & m2b32 ) + ((x >>  2) & m2b32 ); //put count of each  4 bits into those  4 bits
    x = (x & m4b32 ) + ((x >>  4) & m4b32 ); //put count of each  8 bits into those  8 bits
    x = (x & m8b32 ) + ((x >>  8) & m8b32 ); //put count of each 16 bits into those 16 bits
    x = (x & m16b32) + ((x >> 16) & m16b32); //put count of each 32 bits into those 32 bits
    return x;
}

int popcount64c(uint64_t x)
{
    x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
    x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits
    x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits
    return (x * h01) >> 56;  //returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ...
}
//static uint16_t wordbits[65536] = { /* bitcounts of integers 0 through 65535, inclusive */ };
//This algorithm uses 3 arithmetic operations and 2 memory reads.
/*int popcount64e(uint64_t x)
{
    return wordbits[x & 0xFFFF] + wordbits[(x >> 16)& 0xFFFF ]+wordbits[(x >> 32)& 0xFFFF ] + wordbits[x >> 48];
}

int popcount32e(uint32_t x)
{
    return wordbits[x & 0xFFFF] + wordbits[(x >> 16)];
}

//Optionally, the wordbits[] table could be filled using this function
int popcount32e_init(void)
{
    uint32_t i;
    uint16_t x;
    int count;
    for (i=0; i <= 0xFFFF; i++)
    {   wordbits[i] = popcount64c(i);
    }
}*/
// try asm
// try  Brian Kernighan’s Algorithm: https://www.geeksforgeeks.org/count-set-bits-in-an-integer/
// try look up table
// https://en.wikipedia.org/wiki/Hamming_weight

// try const one two three four

int allassociationbinary(int indiv,UNCORING** cases
                                ,UNCORING** controls)
{   int nbgene=100*indiv;
    printf("Genes processed:");
    for(int cgene1=0;cgene1<nbgene-1;cgene1++)
    {   if (cgene1%(nbgene/100)==0) printf(" %d,",cgene1);
        for(int cgene2=cgene1+1;cgene2<nbgene;cgene2++)
        {   int contengencycase [4][4];
            int contengencycontrol [4][4];
            for(int cindiv=0;cindiv<((indiv-1)/SIZETYPE+1);cindiv++)
            {   int deca1=((indiv-1)/SIZETYPE+1);
                int deca2=2*((indiv-1)/SIZETYPE+1);
                int deca3=3*((indiv-1)/SIZETYPE+1);
                contengencycase[0][0]=contengencycase[0][0]+COUNTNBBITS(cases[cgene1][cindiv]&cases[cgene2][cindiv]);
                contengencycontrol[0][0]=contengencycase[0][0]+COUNTNBBITS(controls[cgene1][cindiv]&controls[cgene2][cindiv]);
                contengencycase[0][1]=contengencycase[0][1]+COUNTNBBITS(cases[cgene1][cindiv]&cases[cgene2][cindiv+deca1]);
                contengencycontrol[0][1]=contengencycase[0][1]+COUNTNBBITS(controls[cgene1][cindiv]&controls[cgene2][cindiv+deca1]);
                contengencycase[0][2]=contengencycase[0][2]+COUNTNBBITS(cases[cgene1][cindiv]&cases[cgene2][cindiv+deca2]);
                contengencycontrol[0][2]=contengencycase[0][2]+COUNTNBBITS(controls[cgene1][cindiv]&controls[cgene2][cindiv+deca2]);
                contengencycase[0][3]=contengencycase[0][3]+COUNTNBBITS(cases[cgene1][cindiv]&cases[cgene2][cindiv+deca3]);
                contengencycontrol[0][3]=contengencycase[0][3]+COUNTNBBITS(controls[cgene1][cindiv]&controls[cgene2][cindiv+deca3]);
                contengencycase[1][0]=contengencycase[1][0]+COUNTNBBITS(cases[cgene1][cindiv+deca1]&cases[cgene2][cindiv]);
                contengencycontrol[1][0]=contengencycase[1][0]+COUNTNBBITS(controls[cgene1][cindiv+deca1]&controls[cgene2][cindiv]);
                contengencycase[1][1]=contengencycase[1][1]+COUNTNBBITS(cases[cgene1][cindiv+deca1]&cases[cgene2][cindiv+deca1]);
                contengencycontrol[1][1]=contengencycase[1][1]+COUNTNBBITS(controls[cgene1][cindiv+deca1]&controls[cgene2][cindiv+deca1]);
                contengencycase[1][2]=contengencycase[1][2]+COUNTNBBITS(cases[cgene1][cindiv+deca1]&cases[cgene2][cindiv+deca2]);
                contengencycontrol[1][2]=contengencycase[1][2]+COUNTNBBITS(controls[cgene1][cindiv+deca1]&controls[cgene2][cindiv+deca2]);
                contengencycase[1][3]=contengencycase[1][3]+COUNTNBBITS(cases[cgene1][cindiv+deca1]&cases[cgene2][cindiv+deca3]);
                contengencycontrol[1][3]=contengencycase[1][3]+COUNTNBBITS(controls[cgene1][cindiv+deca1]&controls[cgene2][cindiv+deca3]);
                contengencycase[2][0]=contengencycase[2][0]+COUNTNBBITS(cases[cgene1][cindiv+deca2]&cases[cgene2][cindiv]);
                contengencycontrol[2][0]=contengencycase[2][0]+COUNTNBBITS(controls[cgene1][cindiv+deca2]&controls[cgene2][cindiv]);
                contengencycase[2][1]=contengencycase[2][1]+COUNTNBBITS(cases[cgene1][cindiv+deca2]&cases[cgene2][cindiv+deca1]);
                contengencycontrol[2][1]=contengencycase[2][1]+COUNTNBBITS(controls[cgene1][cindiv+deca2]&controls[cgene2][cindiv+deca1]);
                contengencycase[2][2]=contengencycase[2][2]+COUNTNBBITS(cases[cgene1][cindiv+deca2]&cases[cgene2][cindiv+deca2]);
                contengencycontrol[2][2]=contengencycase[2][2]+COUNTNBBITS(controls[cgene1][cindiv+deca2]&controls[cgene2][cindiv+deca2]);
                contengencycase[2][3]=contengencycase[2][3]+COUNTNBBITS(cases[cgene1][cindiv+deca2]&cases[cgene2][cindiv+deca3]);
                contengencycontrol[2][3]=contengencycase[2][3]+COUNTNBBITS(controls[cgene1][cindiv+deca2]&controls[cgene2][cindiv+deca3]);
                contengencycase[3][0]=contengencycase[3][0]+COUNTNBBITS(cases[cgene1][cindiv+deca3]&cases[cgene2][cindiv]);
                contengencycontrol[3][0]=contengencycase[3][0]+COUNTNBBITS(controls[cgene1][cindiv+deca3]&controls[cgene2][cindiv]);
                contengencycase[3][1]=contengencycase[3][1]+COUNTNBBITS(cases[cgene1][cindiv+deca3]&cases[cgene2][cindiv+deca1]);
                contengencycontrol[3][1]=contengencycase[3][1]+COUNTNBBITS(controls[cgene1][cindiv+deca3]&controls[cgene2][cindiv+deca1]);
                contengencycase[3][2]=contengencycase[3][2]+COUNTNBBITS(cases[cgene1][cindiv+deca3]&cases[cgene2][cindiv+deca2]);
                contengencycontrol[3][2]=contengencycase[3][2]+COUNTNBBITS(controls[cgene1][cindiv+deca3]&controls[cgene2][cindiv+deca2]);
                contengencycase[3][3]=contengencycase[3][3]+COUNTNBBITS(cases[cgene1][cindiv+deca3]&cases[cgene2][cindiv+deca3]);
                contengencycontrol[3][3]=contengencycase[3][3]+COUNTNBBITS(controls[cgene1][cindiv+deca3]&controls[cgene2][cindiv+deca3]);
            }
        }
    }
    return 0;
}

void *cases[MAXGENE];
void *controls[MAXGENE];

int main(int argc, char *argv[])
{   int indiv=10;
    int nbtimetot = 2;
    if (argc<2)
    {   printf("argument missing so ");
    } else
    {   indiv = atoi (argv[1]);
        if (argc>2)
        {   nbtimetot = atoi (argv[2]);
        };
    };
    printf("indiv=%d and nbtest=%d and sizemachine=%d\n",indiv,nbtimetot,((indiv-1)/SIZETYPE+1));
    for (int i=0; i<MAXGENE; i++)
    {   if (i<indiv)
        {   cases[i]    = (UNCORING *) malloc(1);
            controls[i] = (UNCORING *) malloc(1);
        } else
        {   cases[i]    = (UNCORING *) malloc(4*((indiv-1)/SIZETYPE+1) * sizeof(UNCORING));
            controls[i] = (UNCORING *) malloc(4*((indiv-1)/SIZETYPE+1) * sizeof(UNCORING));
        };
    };
    for (int i=0; i<MAXGENE; i++)
    {   if (cases[i] == NULL) printf("inti not ok");
        if (controls[i] == NULL) printf("inti not ok");
    };
    generatedatabinary(indiv,cases,controls);
    printf("generate ok");

    clock_t begin = clock();
    for(int nbtime=0;nbtime<nbtimetot;nbtime++)
    {   printf("\nrun%d\n",nbtime);
    //    popcount32e_init();
        loadcasecontrolbinary(indiv,cases,controls);
        allassociationbinary(indiv,cases,controls);
    };
    clock_t end = clock();
    printf("\nbinary 32 bits in C (%d)\n",indiv);
    float elapsed_secs = (float)(end - begin) / CLOCKS_PER_SEC;
	printf("Mean: %f seconds over %d times",elapsed_secs/nbtimetot,nbtimetot);
  getchar();
    for (int i=0; i<MAXGENE; i++)
    {  // free(cases[i]);
        //free(controls[i]);
    };
    return 0;
}